package com.pwebapp.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {

    private static final long serialVersionUID = 8753202033693333006L;

    private static final Logger logger = LogManager.getLogger(LoginAction.class);

    @Override
    public String execute() throws Exception {
        logger.info("Inside Login Action");
        return Action.SUCCESS;
    }

    public String index() {
        logger.info("Inside Login Index Action");
        return Action.SUCCESS;
    }

}
