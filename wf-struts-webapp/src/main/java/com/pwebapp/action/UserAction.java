package com.pwebapp.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import com.opensymphony.xwork2.ActionSupport;
import com.pwebapp.security.userdetails.WebAppUser;

public class UserAction extends ActionSupport {

    private static final long serialVersionUID = -8501836230707188591L;

    private static final Logger logger = LogManager.getLogger(UserAction.class);

    @Override
    public String execute() throws Exception {
        WebAppUser webUser = (WebAppUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        logger.info("WebApp User :=> " + webUser.getEmail());
        return SUCCESS;
    }
}
