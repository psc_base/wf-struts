package com.pwebapp.test;

import java.sql.Connection;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBConnection {

    private static InitialContext ic = null;

    public static Connection getConnection() throws Exception {
        Connection con = getDataSource().getConnection();
        if (con == null) {
            throw new Exception("No Connection found with Database");
        }
        return con;
    }

    public static DataSource getDataSource() throws Exception {
        ic = new InitialContext();
        if (ic == null) {
            throw new Exception("No initial Context Found");
        }

        DataSource ds = (DataSource) ic.lookup("webapp");
        if (ds == null) {
            throw new Exception("No DataSource Found");
        }

        return ds;
    }

}
