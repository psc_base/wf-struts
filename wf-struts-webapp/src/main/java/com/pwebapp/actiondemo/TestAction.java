package com.pwebapp.actiondemo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;

public class TestAction extends ActionSupport {

    private static final long serialVersionUID = -1155415236313592743L;
    private static final Logger logger = LogManager.getLogger(TestAction.class);

    private String reasonName;
    private String resonCode;

    @Override
    public String execute() throws Exception {
        logger.info("Logger Test");
        logger.info("Reason Name :: " + reasonName + " ; Reason Code :: " + resonCode);
        return SUCCESS;
    }

    @Override
    public void validate() {
        if ((reasonName == null) || "".equals(reasonName)) {
            addActionError("Please enter Reson name");
        }
        if (hasActionErrors()) {
            logger.info("Has Error ");
        }

    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getResonCode() {
        return resonCode;
    }

    public void setResonCode(String resonCode) {
        this.resonCode = resonCode;
    }

}
