package com.pwebapp.actiondemo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import com.opensymphony.xwork2.ActionSupport;
import com.pwebapp.security.CookieClearLogout;

public class LogoutAction extends ActionSupport {

    /**
     * @serial -2524843518176408176L
     */
    private static final long serialVersionUID = -2524843518176408176L;

    private static Logger logger = LogManager.getLogger(LogoutAction.class);

    @Autowired
    private transient CookieClearLogout cookieLogout;

    @Override
    public String execute() throws Exception {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(ServletActionContext.getRequest(),
                    ServletActionContext.getResponse(), auth);
            if (logger.isDebugEnabled()) {
                logger.debug("Servlet Context Path =>" + ServletActionContext.getRequest().getContextPath());
            }
            cookieLogout.cookieClearLogout();
        }
        return SUCCESS;
    }
}
