package com.pwebapp.security;

import javax.servlet.http.Cookie;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;

@Service
public class CookieClearLogoutImpl implements CookieClearLogout {

    @Override
    public void cookieClearLogout() {
        Cookie cookie = new Cookie("PWebApp", null);
        cookie.setSecure(true);
        cookie.setPath(ServletActionContext.getRequest().getContextPath() + "/");
        cookie.setMaxAge(0);
        ServletActionContext.getResponse().addCookie(cookie);
    }
}
