package com.pwebapp.security;

@FunctionalInterface
public interface CookieClearLogout {
    public void cookieClearLogout();
}
