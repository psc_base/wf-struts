package com.pwebapp.security;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component(value = "interActPermissonEvaluator")
public class InterActPermissonEvaluator implements PermissionEvaluator {
    
    private static final Logger logger = LogManager.getLogger(InterActPermissonEvaluator.class);
    
    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        logger.info("In Has Persmisson");
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        return false;
    }

}
