package com.pwebapp.security;

import java.util.Collection;
import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.access.intercept.DefaultFilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.RequestMatcher;

/* @Component(value="securityMetadataSource") */
public class InterActFilterInvocationSecurityMetadataSource extends DefaultFilterInvocationSecurityMetadataSource {

    private static final Logger logger = LogManager.getLogger(InterActFilterInvocationSecurityMetadataSource.class);
    private static LinkedHashMap<RequestMatcher, Collection<ConfigAttribute>> requestMapApp;
    private String[] roles = { "ROLE_ADMIN", "ROLE_ANONYMOUS" };

    public InterActFilterInvocationSecurityMetadataSource() {
        super(requestMapApp);
    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        /*
         * final HttpServletRequest request = ((FilterInvocation)
         * object).getRequest(); RequestMatcher matcher = new
         * AntPathRequestMatcher("/auth/login"); if (matcher.matches(request)) {
         * return SecurityConfig.createList(this.roles);
         * 
         * }
         */
        return null;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return super.getAllConfigAttributes();
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return super.supports(clazz);
    }

}
