package com.pwebapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.util.Assert;

/* @Component */
public class FilterInvocationSecurityMetadataSourcePostProcessor implements BeanPostProcessor {

    @Autowired
    private FilterInvocationSecurityMetadataSource securityMetadataSource;

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if (bean instanceof FilterSecurityInterceptor) {
            ((FilterSecurityInterceptor) bean).setSecurityMetadataSource(securityMetadataSource);
        }
        return bean;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String arg1) {
        return bean;
    }

    public void setSecurityMetadataSource(FilterInvocationSecurityMetadataSource securityMetadataSource) {
        this.securityMetadataSource = securityMetadataSource;
    }

    public void afterPropertiesSet() {
        Assert.notNull(securityMetadataSource, "securityMetadataSource cannot be null");
    }

}
