package com.pwebapp.database.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.pwebapp.entity.Emp;

@Component
public class TestImpl implements Test {

    private static final Logger logger = LogManager.getLogger(TestImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void disp() {
        logger.info("In disp");
        Session session = sessionFactory.getCurrentSession();
        Emp e = (Emp) session.createCriteria(Emp.class).add(Restrictions.eq("empno", new Short("7839"))).uniqueResult();
        logger.info(e);
    }
}
