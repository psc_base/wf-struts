<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="container">
    <div class="row">
        <div class="col-sm-offset-3 col-sm-9">
            <h2 class="pageHeading">
                <span class="glyphicon glyphicon-user"></span> Login
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-offset-3 col-sm-12">
            <s:form action="login" method="post" cssClass="well form-horizontal col-sm-5" theme="bootstrap" role="form">
                <s:textfield id="username" name="username" label="Username" tooltip="Enter your Username" labelCssClass="col-sm-4"
                    elementCssClass="col-sm-7" placeholder="Username" />
                <!-- cssClass="input-sm"  -->
                <s:textfield id="password" type="password" name="password" label="Password" tooltip="Enter your Password"
                    labelCssClass="col-sm-4" elementCssClass="col-sm-7" placeholder="Password" />
                <sec:csrfInput />
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-9">
                        <s:submit id="login" name="Login" cssClass="btn btn-primary"></s:submit>
                    </div>
                </div>
            </s:form>
        </div>
    </div>
</div>
