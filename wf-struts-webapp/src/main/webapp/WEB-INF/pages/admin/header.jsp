<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@taglib prefix="log" uri="http://logging.apache.org/log4j/tld/log"%>
<html>
<head>
<sj:head jqueryui="true" />
</head>
<body>
    <h1>Spring4 Struts2 Spring Security Integration</h1>
    <s:a action="logout" namespace="/">Logout</s:a>
    <log:dump scope="request" />
    <log:info message="This is JSP Log"></log:info>
</body>
</html>
