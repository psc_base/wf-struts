<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<sb:head includeStyles="none" />
<link rel="stylesheet" href="<s:url value="/css/bootstrap.css" />" type="text/css" />
<link rel="stylesheet" href="<s:url value="/css/style.css" />" type="text/css">
<!-- jQuery -->
<script src="<s:url value="/js/jquery.js"/>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<s:url value="/js/bootstrap.min.js"/>"></script>
<title>BooStrap Example</title>
</head>
<body>
    <div class="container-fluid">
        <h2 class="pageHeading">
            <span class="glyphicon glyphicon-file"></span> Registered User's Details
        </h2>
        <div class="row">
            <div class="col-lg-12">
                <fieldset>
                    <legend>Personal Details </legend>
                    <s:form action="index" theme="bootstrap" cssClass="form-horizontal" role="form">
                        <s:property value="%{reasonName}" />
                        <s:property value="%{resonCode}" />
                        <s:textfield id="card-holder-name" name="reasonName" label="Reson Name" tooltip="Enter your Reason Here" />
                        <sec:csrfInput />
                        <s:submit cssClass="btn btn-warning btn-cons" value="SUBMIT"></s:submit>
                    </s:form>
                </fieldset>
            </div>
            <!--  End of form -->
        </div>
        <div class="row">
            <div class="form-group">
                <div class=" text-center tp-padding ">
                    <button type="button" class="btn btn-warning btn-cons">Authorise</button>
                    <button type="button" class="btn btn-warning btn-cons">Hold</button>
                    <button type="button" class="btn btn-danger btn-cons">Reject</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>