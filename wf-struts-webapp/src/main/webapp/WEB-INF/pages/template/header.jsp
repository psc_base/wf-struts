<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<link href="<s:url value='/css/bootstrap.css' encode='false' includeParams='none'/>" rel="stylesheet">
<link href="<s:url value='/css/bootstrap-theme.css' encode='false' includeParams='none'/>" rel="stylesheet">
<link href="<s:url value='/css/main.css' encode='false' includeParams='none'/>" rel="stylesheet">
<link href="<s:url value='/css/pwebapp.css' encode='false' includeParams='none'/>" rel="stylesheet">
<sb:head />