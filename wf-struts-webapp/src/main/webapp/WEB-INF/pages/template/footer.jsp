<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="footer">
    <div class="container-fluid">
        <div class="col-md-12">
            <span class="pull-right">� 2015 PWebApp. All Rights Reserved.</span>
        </div>
    </div>
</div>
<script src="<s:url value='/js/angular.min.js' encode='false' includeParams='none'/>"></script>
<script src="<s:url value='/js/jquery-2.1.4.min.js' encode='false' includeParams='none'/>"></script>
<script src="<s:url value='/js/bootstrap.min.js' encode='false' includeParams='none'/>"></script>
<script src="<s:url value='/js/prettify.js' encode='false' includeParams='none'/>"></script>
